package com.trmsys.hacka.dkc.search.rest.beans;

public class SearchResult {
    private final String title;
    private final String body;
    private final String[] fragment;


    public SearchResult(final String title, final String body, final String[] fragment) {
        this.title = title;
        this.body = body;
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String[] getFragment() {
        return fragment;
    }
}
