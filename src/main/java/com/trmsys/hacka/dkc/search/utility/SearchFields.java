package com.trmsys.hacka.dkc.search.utility;

public final class SearchFields {
    public static final String TITLE = "title";
    public static final String TAGS = "tags";
    public static final String BODY = "body";
}
