package com.trmsys.hacka.dkc.search.rest;

import com.trmsys.hacka.dkc.search.rest.beans.SearchResult;
import com.trmsys.hacka.dkc.search.utility.InMemoryLuceneIndex;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static com.trmsys.hacka.dkc.search.utility.SearchFields.BODY;
import static com.trmsys.hacka.dkc.search.utility.SearchFields.TITLE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class SearchController {


    @RequestMapping(path = "/search", method=GET)
    public List<SearchResult> getDocResults(@RequestParam(value = "term") String term) throws ParseException {
        final List<Document> values = InMemoryLuceneIndex.getInstance().searchIndex(InMemoryLuceneIndex.getInstance().getQuery("title", term));


        //TODO: replace with actual result
        return  values.stream().map( x -> new SearchResult(x.get(TITLE),x.get(BODY), new String[]{})).collect(Collectors.toList());
    }

    @RequestMapping(path = "/searchFormat", method=GET)
    public List<SearchResult> getDocFormattedResults(@RequestParam(value = "term") String term) throws ParseException {
        return InMemoryLuceneIndex.getInstance().searchIndexWithFormatter(InMemoryLuceneIndex.getInstance().getQuery("title", term));
    }
}
