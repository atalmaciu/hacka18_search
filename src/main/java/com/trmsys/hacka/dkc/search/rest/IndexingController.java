package com.trmsys.hacka.dkc.search.rest;


import com.trmsys.hacka.dkc.search.utility.InMemoryLuceneIndex;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.net.URISyntaxException;


import static com.trmsys.hacka.dkc.search.utility.SearchFields.BODY;
import static com.trmsys.hacka.dkc.search.utility.SearchFields.TAGS;
import static com.trmsys.hacka.dkc.search.utility.SearchFields.TITLE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
public class IndexingController {


    @RequestMapping(path = "/insert", method=PUT)
    public void addDocToIndex(@RequestParam(value = TITLE) String title,
                              @RequestParam(value = TAGS) String tags,
                              @RequestParam(value = BODY) String body) throws URISyntaxException {

        InMemoryLuceneIndex.getInstance().indexDocument(title,body);
    }
}
