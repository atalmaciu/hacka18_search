package com.trmsys.hacka.dkc.search.utility;

import com.trmsys.hacka.dkc.search.rest.beans.SearchResult;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.trmsys.hacka.dkc.search.utility.SearchFields.BODY;
import static com.trmsys.hacka.dkc.search.utility.SearchFields.TAGS;
import static com.trmsys.hacka.dkc.search.utility.SearchFields.TITLE;

public class InMemoryLuceneIndex {

    private static FSDirectory FS_INDEX;

    static {
        try {
            FS_INDEX = FSDirectory.open(Paths.get("C:\\temp\\FS_INDEX.lucene"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final static InMemoryLuceneIndex INSTANCE = new InMemoryLuceneIndex(new RAMDirectory(),new StandardAnalyzer());

    public static InMemoryLuceneIndex getInstance(){
        return INSTANCE;
    }

    private Directory memoryIndex;
    private Analyzer analyzer;

    public InMemoryLuceneIndex(Directory memoryIndex, Analyzer analyzer) {
        super();
        this.memoryIndex = memoryIndex;
        this.analyzer = analyzer;
    }

    /**
     *
     * @param title
     * @param body
     */
    public void indexDocument(String title, String body) {

        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        try {
            IndexWriter writter = new IndexWriter(memoryIndex, indexWriterConfig);
            Document document = new Document();

            document.add(new TextField(TITLE, title, Field.Store.YES));
            document.add(new TextField(TAGS, title, Field.Store.YES));
            document.add(new TextField(BODY, body, Field.Store.YES));
            document.add(new SortedDocValuesField(TITLE, new BytesRef(title)));

            writter.addDocument(document);
            writter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Document> searchIndex(String inField, String queryString) {
        try {
            Query query = getQuery(inField, queryString);

            IndexReader indexReader = DirectoryReader.open(memoryIndex);
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(query, 10);
            List<Document> documents = new ArrayList<>();
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                documents.add(searcher.doc(scoreDoc.doc));
            }

            return documents;
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return null;

    }


    public List<SearchResult> searchIndexWithFormatter( Query query) {
        try {
            IndexReader indexReader = DirectoryReader.open(memoryIndex);
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(query, 10);
            List<SearchResult> results = new ArrayList<>();

            Formatter formatter = new SimpleHTMLFormatter();
            QueryScorer scorer = new QueryScorer(query);
            Highlighter highlighter = new Highlighter(formatter, scorer);
            //It breaks text up into same-size texts but does not split up spans
            Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, 300);
            highlighter.setTextFragmenter(fragmenter);


            return Arrays.stream(topDocs.scoreDocs).map(x -> {

                try {
                    Document local = searcher.doc(x.doc);
                    TokenStream stream = TokenSources.getAnyTokenStream(indexReader, x.doc, BODY, analyzer);

                    return new SearchResult(local.get(TITLE),local.get(BODY),highlighter.getBestFragments(stream, local.get(BODY), 10));
                } catch (IOException | InvalidTokenOffsetsException e) {
                    e.printStackTrace();
                }
                return null;
            }).collect(Collectors.toList());




        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public Query getQuery(String inField, String queryString) throws ParseException {
        return new QueryParser(inField, analyzer).parse(queryString);
    }


    public void deleteDocument(Term term) {
        try {
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
            IndexWriter writter = new IndexWriter(memoryIndex, indexWriterConfig);
            writter.deleteDocuments(term);
            writter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Document> searchIndex(Query query) {
        try {
            IndexReader indexReader = DirectoryReader.open(memoryIndex);
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(query, 10);
            List<Document> documents = new ArrayList<>();
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                documents.add(searcher.doc(scoreDoc.doc));
            }

            return documents;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<Document> searchIndex(Query query, Sort sort) {
        try {
            IndexReader indexReader = DirectoryReader.open(memoryIndex);
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(query, 10, sort);
            List<Document> documents = new ArrayList<>();
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                documents.add(searcher.doc(scoreDoc.doc));
            }

            return documents;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }
}
